package transaction_stream

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoUpdate struct {
	Client *mongo.Client
	Col    *mongo.Collection
}

func (m *MongoUpdate) Update(ctx context.Context, entity *TransactionEntity) error {
	// Create new Mongo Session
	session, err := m.Client.StartSession()
	if err != nil {
		return err
	}

	// Start the Transaction in current Session
	if err := session.StartTransaction(); err != nil {
		return err
	}

	if err := mongo.WithSession(ctx, session, func(sc mongo.SessionContext) error {
		for _, item := range entity.Items {
			if _, err := m.Col.UpdateOne(sc, bson.M{"_id": item.ItemId}, bson.M{
				"$inc": bson.M{"quantity": item.Quantity * -1},
			}); err != nil {
				_ = session.AbortTransaction(sc)
				return err
			}
		}

		return nil
	}); err != nil {
		return err
	}

	if err := session.CommitTransaction(ctx); err != nil {
		return err
	}

	session.EndSession(ctx)

	return nil
}
