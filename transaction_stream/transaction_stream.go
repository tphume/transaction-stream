package transaction_stream

import (
	"bytes"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

const opInsert = "insert"

// Represent a transaction document
type TransactionEntity struct {
	TransactionId string `json:"transaction_id" bson:"_id"`
	UserId        string `json:"-" bson:"user_id"`
	StoreId       string `json:"store_id" bson:"store_id"`
	Items         []Item `json:"items" bson:"items"`
}

type Item struct {
	ItemId    string `json:"item_id" bson:"item_id"`
	ProductId string `json:"product_id" bson:"product_id"`
	Quantity  int    `json:"quantity" bson:"quantity"`
}

//  Represent data from Mongo Change Stream
type MongoStreamEntity struct {
	FullDocument  TransactionEntity `bson:"fullDocument"`
	OperationType string            `bson:"operationType"`
}

// Is used to update data to the data source
type UpdateRepo interface {
	Update(ctx context.Context, entity *TransactionEntity) error
}

// Persist the resume token to some storage
type TokenRepo interface {
	Write(ctx context.Context, token []byte) error
	Token(ctx context.Context) ([]byte, error)
}

// Watch MongoDB Change Stream
func WatchMongoStream(ctx context.Context, col *mongo.Collection, update UpdateRepo, token TokenRepo) error {
	// Retrieve the latest resume token first
	log.Print("Retrieving resume token")

	tk, err := token.Token(ctx)
	if err != nil {
		return err
	}

	// Create ChangeStream
	log.Println("Creating Change Stream")

	ops := make([]*options.ChangeStreamOptions, 0)
	if len(tk) != 0 {
		temp, err := bson.NewFromIOReader(bytes.NewReader(tk))
		if err != nil {
			return err
		}

		ops = append(ops, options.ChangeStream().SetResumeAfter(temp))
	}

	cs, err := col.Watch(ctx, mongo.Pipeline{}, ops...)
	if err != nil {
		return err
	}

	// Start reading stream
	log.Println("Start watching stream")

	for cs.Next(ctx) {
		// Decode into entity
		streamEntity := &MongoStreamEntity{}

		err := cs.Decode(streamEntity)
		if err != nil {
			return err
		}

		// Propagate update
		if streamEntity.OperationType == opInsert {
			if err = update.Update(ctx, &streamEntity.FullDocument); err != nil {
				return err
			}

			log.Printf("Updated item %s\n", streamEntity.FullDocument.StoreId)
		}

		// Update token
		if err = token.Write(ctx, cs.ResumeToken()); err != nil {
			return err
		}
	}

	return nil
}
