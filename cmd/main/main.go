package main

import (
	"context"
	"github.com/spf13/viper"
	"gitlab.com/esd-gpos/transaction-stream/transaction_stream"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"strings"
	"time"
)

const (
	filename   = "token"
	configFile = "main.env"
	configPath = "."
)

func main() {
	// Load config
	loadConfig(configFile, configPath)

	// Get env variables
	mongoTranUri := viper.GetString("MONGO_TRAN_URI")
	mongoItemUri := viper.GetString("MONGO_ITEM_URI")

	failOnEmpty(mongoTranUri, mongoItemUri)

	// Connect to Mongo Tran collection
	mongoTranClient, err := mongo.NewClient(options.Client().ApplyURI(mongoTranUri))
	failOnError("could not create mongo client", err)

	timeout, cancel := context.WithTimeout(context.Background(), time.Second*20)
	defer cancel()

	err = mongoTranClient.Connect(timeout)
	failOnError("mongo client could not connect", err)

	mongoTranCollection := mongoTranClient.Database("production").Collection("transaction")

	// Connect to Mongo Item collection
	mongoItemClient, err := mongo.NewClient(options.Client().ApplyURI(mongoItemUri))
	failOnError("could not create mongo client", err)

	timeout, cancel = context.WithTimeout(context.Background(), time.Second*20)
	defer cancel()

	err = mongoItemClient.Connect(timeout)
	failOnError("mongo client could not connect", err)

	mongoItemCollection := mongoItemClient.Database("production").Collection("item")

	// Create Repo
	updateItem := &transaction_stream.MongoUpdate{Col: mongoItemCollection, Client: mongoItemClient}
	fileToken := &transaction_stream.FileTokenRepo{Filename: filename}

	log.Println("Begin")
	log.Fatal(transaction_stream.WatchMongoStream(context.Background(), mongoTranCollection, updateItem, fileToken))
}

func loadConfig(filename string, path string) {
	viper.SetConfigFile(filename)
	viper.AddConfigPath(path)

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("could not read config")
	}
}

func failOnEmpty(value ...string) {
	for _, v := range value {
		if strings.TrimSpace(v) == "" {
			log.Fatal("some value is missing")
		}
	}
}

func failOnError(msg string, err error) {
	if err != nil {
		log.Fatalf("%s:%s", msg, err)
	}
}
